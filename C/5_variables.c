/* simple math operations */

#include <stdio.h>
int main() {
	int  a,b ;
	float salary = 56.78;
	char letter = 'F';
	a = 8;
	b = 34;
	int c = a+b;

	printf("%d \n", c);
	printf("%f \n", salary);
	printf("%c \n", letter);
	printf("\n");
	// integers
	int d;
	int e;
	int f,g;
	d = 23;
	e = 45;
	f = 4;
	g = 3;
	printf("integers are ------> \n");
	printf("sum of d and e is = %d \n", d+e);
	printf("the value of f is %d \n",f);
	printf("after adding d,e,f,j we got = %d \n", d+e+f+g);
	printf("\n");
	// Float 
	float h,i,j,k;
	h = 43.53;
	i = 5.54;
	j = 5.8;
	k = 2.7;
	printf("Floats are -------------> \n");
	printf("lets add up h and i together : %f) \n", h+i);
	printf("value of j is %f \n",j);
	printf("\n");
	// Char
	char p;
	char q;
	char r;
	p = 'F';
	q = 'R';
	r = 'E';
	char s = 'Y';
	printf("who am i ? ----------->\n");
	printf("you are ---------------> \n");
	printf("\n");
	printf("\n");
	printf("%c \n", p);
	printf("%c \n", q);
	printf("%c \n", r);
	printf("%c \n", s);
	printf("\n");
	return 0;
}



