/* 
 * DATE - 2-11-2021
 * Constants part 6 of Fr3y/CSE/C
 * :)
*/

// this is not going to change in the whole program 

#include <stdio.h>
#define p printf
#define Frey 666
int main() {
	const double PI = 3.14;
	p("%f \n", PI);
	p("FreySec \n");
	p("Hello %li \n", Frey);
	
	return 0;
}
